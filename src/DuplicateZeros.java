public class DuplicateZeros {
    static void printArrey(int[] arrey) {
        System.out.print("[");
        for (int i = 0; i < arrey.length; i++) {
            System.out.print(arrey[i]);
            if(i<arrey.length-1){
                System.out.print(",");
            }
        }
        System.out.print("]");
        System.out.println();
    }
    static void checkArrey(int[] arrey) {
        for (int i = arrey.length - 2; i >= 0; i--) {
            if (arrey[i] == 0) {
                for (int j = arrey.length - 2; j > i; j--) {
                    arrey[j + 1] = arrey[j];
                }
                arrey[i + 1] = 0;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        int[] arr = { 1, 0, 2, 3, 0, 4, 5, 0 };
        int[] arr1 = { 1, 2, 3, 4, 5};
        System.out.print("Array Original = ");
        printArrey(arr);
        checkArrey(arr);
        System.out.print("Array Dubplicate 0 = ");
        printArrey(arr);
        System.out.print("Array Original = ");
        printArrey(arr1);
        checkArrey(arr1);
        System.out.print("Array Duplicate 0 = ");
        printArrey(arr1);
    }
}